package com.example.pokemonapp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pokemonapp.interfaces.PokemonInterface;
import com.example.pokemonapp.models.AbilitiesModel;
import com.example.pokemonapp.models.HeldItemsModel;
import com.example.pokemonapp.models.PokemonModel;
import com.example.pokemonapp.models.StatModel;
import com.example.pokemonapp.models.TypesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewPokemon extends AppCompatActivity {
    TextView pokemonName;
    TextView height;
    TextView weight;
    ArrayList<String> abilities = new ArrayList<String>();
    ArrayList<String> heldItems = new ArrayList<String>();
    ArrayList<String> types = new ArrayList<String>();
    ArrayAdapter adapterAbilities = null;
    ArrayAdapter adapterHeldItems = null;
    BaseAdapter adapterTypes = null;
    ListView listAbilities = null;
    ListView listHeldItems = null;
    ImageView image = null;
    ProgressBar progressBarHP = null;
    ProgressBar progressBarAttack = null;
    ProgressBar progressBarDefense = null;
    ProgressBar progressBarSA = null;
    ProgressBar progressBarSD = null;
    ProgressBar progressBarSpeed = null;
    GridView gridViewTypes = null;
    String urlImage;
    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pokemon);
        getPokemonInformation(getIntent().getExtras().getString("name"));
        listAbilities = (ListView) findViewById(R.id.listViewAbilities);
        listHeldItems = (ListView) findViewById(R.id.listViewHeldItems);
        gridViewTypes = (GridView) findViewById(R.id.gvTypes);
        adapterAbilities = new ArrayAdapter(context, android.R.layout.simple_list_item_1, abilities);
        adapterHeldItems = new ArrayAdapter(context, android.R.layout.simple_list_item_1, heldItems);
        adapterTypes = new ArrayAdapter(context, android.R.layout.simple_list_item_1, types);
        listAbilities.setAdapter(adapterAbilities);
        listHeldItems.setAdapter(adapterHeldItems);
        gridViewTypes.setAdapter(adapterTypes);
    }

    private void getPokemonInformation(String name){
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        PokemonInterface pokemonInterface =  retrofit.create(PokemonInterface.class);
        Call<PokemonModel> call = pokemonInterface.getPokemonByName(name);
        call.enqueue(new Callback<PokemonModel>() {
            @Override
            public void onResponse(Call<PokemonModel> call, Response<PokemonModel> response) {
                setPokemonInformation(response.body());
            }

            @Override
            public void onFailure(Call<PokemonModel> call, Throwable t) {
                System.out.println("Algo salio mal "+t);
                Toast toast = Toast.makeText(context, "Algo falló al obtener información", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    public void setPokemonInformation(PokemonModel pokemon){
        //asignando id's
        pokemonName = findViewById(R.id.lblPokemon);
        image = findViewById(R.id.imgPokemon);
        height = findViewById(R.id.txtheight);
        weight = findViewById(R.id.txtweight);
        listAbilities = findViewById(R.id.listViewAbilities);
        listHeldItems =  findViewById(R.id.listViewHeldItems);
        progressBarHP = findViewById(R.id.pbhp);
        progressBarAttack = findViewById(R.id.pbAttack);
        progressBarDefense = findViewById(R.id.pbDefense);
        progressBarSA = findViewById(R.id.pbSA);
        progressBarSD = findViewById(R.id.pbSD);
        progressBarSpeed = findViewById(R.id.pbSpeed);
        gridViewTypes = findViewById(R.id.gvTypes);
        //set
        String id = String.format("%0" + 3 + "d", Integer.valueOf(pokemon.getId().toString()));
        pokemonName.setText(id+". "+ pokemon.getName());
        height.setText(pokemon.getHeight()/10+" m");
        weight.setText(pokemon.getWeight()/10+" kg");
        ArrayList<AbilitiesModel> arrayAbilities = pokemon.getAbilities();
        if(arrayAbilities.isEmpty()){
            abilities.add("No abilities");
        }else{
            arrayAbilities.forEach((n) ->{
                String oculto = "";
                if(n.getIs_hidden()) oculto=" (Hidden)";
                abilities.add(n.getAbility().getName()+oculto);
            });
        }
        ArrayList<HeldItemsModel> arrayHeldItems = pokemon.getHeld_items();
        if(arrayHeldItems.isEmpty()){
            heldItems.add("No items");
        }else{
            arrayHeldItems.forEach((n) -> heldItems.add(n.getItem().getName()));
        }
        //llenando stats
        ArrayList<StatModel> arrayStats = pokemon.getStats();
        arrayStats.forEach((n) -> {
            switch (n.getStat().getName()){
                case "hp": progressBarHP.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarHP);
                    break;
                case "attack": progressBarAttack.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarAttack);
                    break;
                case "defense": progressBarDefense.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarDefense);
                    break;
                case "special-attack": progressBarSA.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarSA);
                    break;
                case "special-defense": progressBarSD.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarSD);
                    break;
                case "speed": progressBarSpeed.setProgress(n.getBase_stat());
                    setColorProgressBar(n.getBase_stat(), progressBarSpeed);
                    break;
            }
        });
        urlImage = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pokemon.getId().toString()+".png";
        Picasso.
                with(context).
                load(urlImage).
                into(image);
        ArrayList<TypesModel> arrayTypes = pokemon.getTypes();
        if(arrayTypes.isEmpty()){
            types.add("No types");
        }else{
            arrayTypes.forEach((n) -> types.add(n.getType().getName()));
        }
        adapterAbilities.notifyDataSetChanged();
        adapterHeldItems.notifyDataSetChanged();
        adapterTypes.notifyDataSetChanged();
    }

    private void setColorProgressBar(Integer base, ProgressBar progressBar){
        if(base<50) progressBar.getProgressDrawable().setColorFilter(Color.RED,android.graphics.PorterDuff.Mode.SRC_IN);
        if(base>=50 && base<=60) progressBar.getProgressDrawable().setColorFilter(Color.YELLOW,android.graphics.PorterDuff.Mode.SRC_IN);
        if(base>60) progressBar.getProgressDrawable().setColorFilter(Color.GREEN,android.graphics.PorterDuff.Mode.SRC_IN);
    }
}