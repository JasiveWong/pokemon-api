package com.example.pokemonapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.pokemonapp.interfaces.PokemonInterface;
import com.example.pokemonapp.models.AbilitieModel;
import com.example.pokemonapp.models.PokemonsListModel;

import androidx.appcompat.app.AppCompatActivity;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.pokemonapp.databinding.ActivityMainBinding;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    ArrayList<String> pokemons = new ArrayList<String>();
    ListView listPokemons = null;
    ArrayAdapter adaptador = null;
    EditText txtPokemon;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        listPokemons = (ListView) findViewById(R.id.listview);
        txtPokemon = (EditText) findViewById(R.id.txtPokemon);
        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, pokemons);
        listPokemons.setAdapter(adaptador);
        getAllPokemons();

        txtPokemon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adaptador.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        listPokemons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String str = listPokemons.getAdapter().getItem(i).toString();
                Intent intent = new Intent(getApplicationContext(), ViewPokemon.class);
                intent.putExtra("name", str);
                startActivity(intent);
            }
        });


    }

    private void getAllPokemons(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        PokemonInterface pokemonInterface =  retrofit.create(PokemonInterface.class);
        Call<PokemonsListModel> call = pokemonInterface.findAll();
        call.enqueue(new Callback<PokemonsListModel>() {
            @Override
            public void onResponse(Call<PokemonsListModel> call, Response<PokemonsListModel> response) {
                ArrayList<AbilitieModel> lista = response.body().getResults();
                lista.forEach((n) -> pokemons.add(n.getName()));
                adaptador.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PokemonsListModel> call, Throwable t) {
                System.out.println("Algo salio mal "+t);
                Toast toast = Toast.makeText(context, "Algo falló al obtener información", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
}