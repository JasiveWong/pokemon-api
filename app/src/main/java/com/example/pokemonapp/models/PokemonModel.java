package com.example.pokemonapp.models;


import java.util.ArrayList;

public class PokemonModel {
    private ArrayList<AbilitiesModel> abilities;
    private Number baseExperience;
    private ArrayList<GameIndexesModel> game_indices;
    private float height;
    private ArrayList<HeldItemsModel> held_items;
    private Number id;
    private String name;
    private float weight;
    private ArrayList<StatModel> stats;
    private ArrayList<TypesModel> types;

    public ArrayList<AbilitiesModel> getAbilities() {
        return abilities;
    }

    public void setAbilities(ArrayList<AbilitiesModel> abilities) {
        this.abilities = abilities;
    }

    public Number getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(Number baseExperience) {
        this.baseExperience = baseExperience;
    }

    public ArrayList<GameIndexesModel> getGame_indices() {
        return game_indices;
    }

    public void setGame_indices(ArrayList<GameIndexesModel> game_indices) {
        this.game_indices = game_indices;
    }

    public ArrayList<StatModel> getStats() {
        return stats;
    }

    public void setStats(ArrayList<StatModel> stats) {
        this.stats = stats;
    }

    public ArrayList<HeldItemsModel> getHeld_items() {
        return held_items;
    }

    public void setHeld_items(ArrayList<HeldItemsModel> held_items) {
        this.held_items = held_items;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public ArrayList<TypesModel> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<TypesModel> types) {
        this.types = types;
    }
}
