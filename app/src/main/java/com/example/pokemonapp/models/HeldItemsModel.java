package com.example.pokemonapp.models;

public class HeldItemsModel {
    private AbilitieModel item;
    private VersionDetailsModel versionDetails;

    public AbilitieModel getItem() {
        return item;
    }

    public void setItem(AbilitieModel item) {
        this.item = item;
    }

    public VersionDetailsModel getVersionDetails() {
        return versionDetails;
    }

    public void setVersionDetails(VersionDetailsModel versionDetails) {
        this.versionDetails = versionDetails;
    }
}
