package com.example.pokemonapp.models;

public class StatModel {
    private Integer base_stat;
    private Number effort;
    private AbilitieModel stat;

    public Integer getBase_stat() {
        return base_stat;
    }

    public void setBase_stat(Integer base_stat) {
        this.base_stat = base_stat;
    }

    public Number getEffort() {
        return effort;
    }

    public void setEffort(Number effort) {
        this.effort = effort;
    }

    public AbilitieModel getStat() {
        return stat;
    }

    public void setStat(AbilitieModel stat) {
        this.stat = stat;
    }
}
