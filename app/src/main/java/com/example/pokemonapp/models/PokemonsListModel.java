package com.example.pokemonapp.models;

import java.util.ArrayList;

public class PokemonsListModel {
    private Number count;
    private String Next;
    private String Previous;
    private ArrayList<AbilitieModel> results;

    public Number getCount() {
        return count;
    }

    public void setCount(Number count) {
        this.count = count;
    }

    public String getNext() {
        return Next;
    }

    public void setNext(String next) {
        Next = next;
    }

    public String getPrevious() {
        return Previous;
    }

    public void setPrevious(String previous) {
        Previous = previous;
    }

    public ArrayList<AbilitieModel> getResults() {
        return results;
    }

    public void setResults(ArrayList<AbilitieModel> results) {
        this.results = results;
    }
}
