package com.example.pokemonapp.models;

public class AbilitiesModel {
    private AbilitieModel ability;
    private Boolean is_hidden;
    private Number slot;

    public AbilitieModel getAbility() {
        return ability;
    }

    public void setAbility(AbilitieModel ability) {
        this.ability = ability;
    }

    public Number getSlot() {
        return slot;
    }

    public void setSlot(Number slot) {
        this.slot = slot;
    }

    public Boolean getIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(Boolean is_hidden) {
        this.is_hidden = is_hidden;
    }
}
