package com.example.pokemonapp.models;

public class GameIndexesModel {
    private Number gameIndex;
    private AbilitieModel version;

    public Number getGameIndex() {
        return gameIndex;
    }

    public void setGameIndex(Number gameIndex) {
        this.gameIndex = gameIndex;
    }

    public AbilitieModel getVersion() {
        return version;
    }

    public void setVersion(AbilitieModel version) {
        this.version = version;
    }
}
