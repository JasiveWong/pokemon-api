package com.example.pokemonapp.models;

public class VersionDetailsModel {
    private Number rarity;
    private AbilitieModel version;

    public Number getRarity() {
        return rarity;
    }

    public void setRarity(Number rarity) {
        this.rarity = rarity;
    }

    public AbilitieModel getVersion() {
        return version;
    }

    public void setVersion(AbilitieModel version) {
        this.version = version;
    }
}
