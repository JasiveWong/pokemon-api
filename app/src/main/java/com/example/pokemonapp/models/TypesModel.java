package com.example.pokemonapp.models;

public class TypesModel {
    private AbilitieModel type;

    public AbilitieModel getType() {
        return type;
    }

    public void setType(AbilitieModel type) {
        this.type = type;
    }
}
