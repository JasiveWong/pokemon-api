package com.example.pokemonapp.interfaces;

import com.example.pokemonapp.models.PokemonModel;
import com.example.pokemonapp.models.PokemonsListModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokemonInterface {
    @GET("pokemon/{name}")
    Call<PokemonModel> getPokemonByName(@Path("name") String name);

    @GET("pokemon?limit=100000&offset=0")
    Call<PokemonsListModel> findAll();
}
